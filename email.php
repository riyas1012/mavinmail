<?php

if($_SERVER['REQUEST_METHOD']=="POST")
{
	user_contact();
}
else 
{
	$response=array('status' => "failed",'reason'=>"Use POST method");
	echo  utf8_decode(json_encode($response,JSON_FORCE_OBJECT));
}

function user_contact()
{
	if(!empty($_POST['full_name']))
	{
		if(!empty($_POST['from']))
		{
			if(!empty($_POST['message']))
			{
				$from=$_POST['from'];
				$full_name=$_POST['full_name'];
				$message=$_POST['message'];
				$to='sunderrajanr@mavinapps.com';
				$subject="Contact";
				$message = $message;
				$headers = "From:www.mavinapps.com \r\n";
				$retval = mail ($to,$subject,$message,$from);
				if( $retval == true )
				{
					$response=array('status' => "success",'response'=>"An email has been sent to your email address successfully.");
					echo  utf8_decode(json_encode($response,JSON_FORCE_OBJECT));
				}
				else
				{
					$response=array('status' => "failed",'reason'=>"email has not been sent successfully");
					echo  utf8_decode(json_encode($response,JSON_FORCE_OBJECT));
				}

			}
			else
			{
				$response=array('status' => "failed",'reason'=>"message field is required");
				echo  utf8_decode(json_encode($response,JSON_FORCE_OBJECT));
			}
		}
		else
		{
			$response=array('status' => "failed",'reason'=>"from field is required");
			echo  utf8_decode(json_encode($response,JSON_FORCE_OBJECT));
		}
	}
	else
	{
		$response=array('status' => "failed",'reason'=>"full name field is required");
		echo  utf8_decode(json_encode($response,JSON_FORCE_OBJECT));
	}
	
}	
?>